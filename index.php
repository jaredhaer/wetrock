<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
    <head>
        <!-- Responsive layout meta tags, character set, and compatibility -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Import of stylesheet for the template -->
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template.css" type="text/css" />
        
        <!-- Import of template javascript -->
        <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/main.js" type="text/javascript"></script>

         <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <!-- Import head data, description and title from Joomla -->
        <jdoc:include type="head" />
        
        <!-- Get parameters for template, I HOPE TITLE IS ONE OF THEM -->
        <?php $app = JFactory::getApplication();
        $tplparams = $app->getTemplate(true)->params; ?>
        
    </head>
    <body class="bg-3">
    <!-- Main container, top level div tag -->
    <div class="container">
        <!-- Navigation/Menu top level tag -->
         <div class="row">
            <nav class="navbar navbar-inverse navbar-fixed-top">
              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="index.php">WHETROCK QUARRIES</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                  <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="products">Products</a></li>
                    <li><a href="about-us">About</a></li>
                    <li><a href="contact">Contact</a></li>
                  </ul>
                </div>
              </div>
            </nav>
        </div>
        </br>
        </br>
        </br>
        </br>
      
        <div class="row">
            <div class="col-lg-4">
                <!-- Image/logo from template -->
                <image src="<?php $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/logo.png" alt="Rock truck" class="responsive" />
            </div>
                <div class="col-lg-6">
                    <!-- Main article/content -->
                    <jdoc:include type="component" />
            </div>
            <div class="col-lg-2">
            &nbsp;
            </div>
        </div>
        
        <!-- Joomla bottom content -->
        <div class="container-fluid">
        <jdoc:include type="modules" name="bottom" />
        </div>
    </div>
        
        <!-- import jquery min from google API -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
